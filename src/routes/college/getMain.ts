import getFirstInstitutions from "../../repository/institution/getFirst";

const getMain = async (router) => {
  return router.post("/main", async (request, response) => {
    const result = await getFirstInstitutions();
    response.status(200).send(result);
  });
};

export default getMain;
