import institutionSearch from "../../repository/institution/search";

const collegeSearch = async (router) => {
  return router.post("/search", async (request, response) => {
    const searchQuery = await request.body.searchQuery;
    const result = await institutionSearch(searchQuery);
    response.status(200).send(result);
  });
};

export default collegeSearch;
