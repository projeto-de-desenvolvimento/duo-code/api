const express = require("express");
const router = express.Router();

import search from "./search";
import getMain from "./getMain";

search(router);
getMain(router);

module.exports = router;
