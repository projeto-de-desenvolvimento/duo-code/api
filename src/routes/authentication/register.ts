const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

import { getRepository, getConnection, Connection } from "typeorm";
import { User } from "../../entity/User";

const register = async (request, response) => {
  const username = request.body.username;
  const email = request.body.email;
  const password = request.body.password;

  if (!username || !email || !password) {
    response.status(400).send("Dados incompletos");
  }

  try {
    const usernameAlreadyExist = await getRepository(User)
      .createQueryBuilder()
      .where("username = :username", { username: username })
      .getOne();

    const emailAlreadyExist = await getRepository(User)
      .createQueryBuilder()
      .where("email = :email", { email: email })
      .getOne();

    if (!usernameAlreadyExist && !emailAlreadyExist) {
      await bcrypt.hash(password, 15, async (err, hash) => {
        if (err) response.status(500).send("Ocorreu um erro no registro");
        await getConnection()
          .createQueryBuilder()
          .insert()
          .into(User)
          .values([
            { username: username, email: email, password: hash, offensive: 0 },
          ])
          .execute();
        response.status(200).send("Usuário Criado.");
      });
    } else {
      let errors = [];
      if (usernameAlreadyExist) errors.push("Username já existe.");
      if (emailAlreadyExist) errors.push("E-mail já existe.");
      response.status(400).send({ errors });
    }
  } catch (e) {
    response.status(500).send("Ocorreu um erro no registro");
  }
};

export default register;
