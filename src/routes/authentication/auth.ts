const express = require('express');
const router = express.Router();
const bodyParser = require("body-parser");
const jwt = require('jsonwebtoken');

import Login from './login'
import Register from './register'

router.post('/login', Login);

router.post('/register', Register);

module.exports = router;