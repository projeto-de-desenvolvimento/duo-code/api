import { getRepository } from "typeorm";
import { User } from "../../entity/User";
const jwt = require("jsonwebtoken");

const middleware = async (req, res, next) => {
  try {
    const [, token] = req.headers.authorization.split(" ");
    const payload = await jwt.verify(token, process.env.SECRET_KEY);
    const userEmail = payload.user.email;
    const user = await getRepository(User)
      .createQueryBuilder()
      .where("email = :email", { email: userEmail })
      .getOne();
    if (user === undefined) {
      return res.status(401).send();
    } else {
      req.auth = user;

      next();
    }
  } catch (error) {
    res.status(401).send(error);
  }
};

export default middleware;
