import getUserByEmail from "../../repository/user/getUserByEmail";
import deleteAsk from "../../repository/asking/deleteAsking";
const jwt = require("jsonwebtoken");

const deleteAsking = (router) => {
  return router.delete("/:id", async (request, response) => {
    const askingId = request.params.id;

    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = payload.user.email;

    const user = await getUserByEmail(email);

    if (user) {
      deleteAsk(askingId)
        .then(() => {
          response.status(200).send("Pergunta deletada com sucesso.");
        })
        .catch(() => {
          response.status(500).send("Erro ao deletar a pergunta.");
        });
    } else {
      response.status(500).send("Erro de chave de autenticação.");
    }
  });
};

export default deleteAsking;
