import getUserByEmail from "../../repository/user/getUserByEmail";
import insertAsking from "../../repository/asking/insertAsking";
const jwt = require("jsonwebtoken");

const createQuestion = (router) => {
  return router.post("/", async (request, response) => {
    const title = request.body.title;
    const content = request.body.content;
    const tags = request.body.tags;

    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = payload.user.email;

    const user = await getUserByEmail(email);

    if (user) {
      insertAsking(title, content, tags, user)
        .then(() => {
          response.status(200).send("Pergunta criada com sucesso.");
        })
        .catch(() => {
          response.status(500).send("Erro ao criar a pergunta.");
        });
    } else {
      response.status(500).send("Erro de chave de autenticação.");
    }
  });
};

export default createQuestion;
