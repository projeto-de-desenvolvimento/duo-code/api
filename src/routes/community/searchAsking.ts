import searchAsking from "../../repository/community/searchAskings";

const searchAskingRoute = (router) => {
  return router.get(
    "/spotlightAskings/search/:query",
    async (request, response) => {
      const query = request.params.query;
      if (!query) response.status(404);

      const data = await searchAsking(query);

      if (data) {
        response.status(200).send(data);
        return;
      }

      response.status(404).send();
    }
  );
};

export default searchAskingRoute;
