import getUserByEmail from "../../repository/user/getUserByEmail";
import insertReviewRating from "../../repository/reviewRating/insert";
import getComment from "../../repository/comment/get";
const jwt = require("jsonwebtoken");

const createReviewRating = (router) => {
  return router.post("/", async (request, response) => {
    const like = request.body.like;
    const commentId = request.body.commentId;

    const user = await getUser(request);
    const comment = await getComment(commentId);

    if (user && comment) {
      insertReviewRating(like, user, comment)
        .then(async () => {
          const comment = await getComment(commentId);

          response
            .status(200)
            .send({ rating: { like: comment.like, dislike: comment.dislike } });
        })
        .catch(() => {
          response.status(500).send("Erro ao avaliar.");
        });
    } else {
      response.status(500).send("Ocorreu um erro ao avaliar.");
    }
  });
};

const getUser = async (request) => {
  const [, token] = request.headers.authorization.split(" ");
  const payload = jwt.verify(token, process.env.SECRET_KEY);
  const email = payload.user.email;
  const user = await getUserByEmail(email);

  return user;
};

export default createReviewRating;
