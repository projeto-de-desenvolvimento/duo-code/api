const express = require("express");
const router = express.Router();

import create from "./create";
import remove from "./remove";
import get from "./get";

create(router);
remove(router);
get(router);

module.exports = router;
