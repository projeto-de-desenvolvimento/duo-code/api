import getUserByEmail from "../../repository/user/getUserByEmail";
import deleteReviewRating from "../../repository/reviewRating/delete";
const jwt = require("jsonwebtoken");

const deleteReviewRatingById = (router) => {
  return router.delete("/:id", async (request, response) => {
    const id = request.params.id;

    const user = await getUser(request);

    if (user) {
      deleteReviewRating(id)
        .then(() => {
          response.status(200).send("Avaliação deletado com sucesso.");
        })
        .catch(() => {
          response.status(500).send("Erro ao deletar o avaliação.");
        });
    } else {
      response.status(500).send("Erro de chave de autenticação.");
    }
  });
};

const getUser = async (request) => {
  const [, token] = request.headers.authorization.split(" ");
  const payload = jwt.verify(token, process.env.SECRET_KEY);
  const email = payload.user.email;
  const user = await getUserByEmail(email);

  return user;
};

export default deleteReviewRatingById;
