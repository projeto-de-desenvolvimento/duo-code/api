import getUserByEmail from "../../repository/user/getUserByEmail";
import insertComment from "../../repository/comment/insert";
import getAsking from "../../repository/asking/getAsking";
const jwt = require("jsonwebtoken");

const createQuestion = (router) => {
  return router.post("/", async (request, response) => {
    const content = request.body.content;
    const askingId = request.body.askingId;

    const user = await getUser(request);
    const asking = await getAsking(askingId);

    if (user && asking.active) {
      insertComment(content, asking, user)
        .then(() => {
          response.status(200).send("Comentário criado com sucesso.");
        })
        .catch(() => {
          response.status(500).send("Erro ao comentar.");
        });
    } else {
      response.status(500).send("Ocorreu um erro ao comentar.");
    }
  });
};

const getUser = async (request) => {
  const [, token] = request.headers.authorization.split(" ");
  const payload = jwt.verify(token, process.env.SECRET_KEY);
  const email = payload.user.email;
  const user = await getUserByEmail(email);

  return user;
};

export default createQuestion;
