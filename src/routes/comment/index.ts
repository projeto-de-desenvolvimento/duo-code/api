const express = require("express");
const router = express.Router();

import createAsking from "./create";
import deleteAsking from "./softDelete";
import getAsking from "./get";
import updateAsking from "./update";

createAsking(router);
deleteAsking(router);
getAsking(router);
updateAsking(router);

module.exports = router;
