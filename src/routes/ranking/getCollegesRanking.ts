import { getRepository } from "typeorm";
import { Institution } from "../../entity/Institution";

const getCollegesRanking = async (router) => {
  return router.get("/colleges/:page", async (request, response) => {
    const page = request.params.page - 1;
    const min = page * 10;
    const max = min + 10;
    const ranking = await getRepository(Institution)
      .createQueryBuilder()
      .orderBy("totalXp", "DESC")
      .skip(min)
      .take(max)
      .getMany();

    response.status(200).send(ranking);
  });
};

export default getCollegesRanking;
