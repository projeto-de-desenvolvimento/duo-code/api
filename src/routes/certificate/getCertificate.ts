import { getRepository } from "typeorm";
import { Certificate } from "../../entity/Certificate";
import getCourseById from "../../repository/course/getCourseById";
import getUserByEmail from "../../repository/user/getUserByEmail";
const jwt = require("jsonwebtoken");

const getCertificate = async (router) => {
  return router.get("/:hash", async (request, response) => {
    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = payload.user.email;
    const user = await getUserByEmail(email);

    const hash = await request.params.hash;

    const result = await getRepository(Certificate)
      .createQueryBuilder("certificate")
      .where("certificate.hash = :hash", { hash })
      .leftJoinAndSelect("certificate.course", "course")
      .leftJoinAndSelect("certificate.user", "user")
      .getOne();

    response.status(200).send(result);
  });
};

export default getCertificate;
