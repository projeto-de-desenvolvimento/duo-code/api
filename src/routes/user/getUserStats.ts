import getUserByEmail from "../../repository/user/getUserByEmail";
import getNumberOfCourses from "../../repository/user/getNumberOfCourses";
const jwt = require("jsonwebtoken");

const getUserStats = async (router) => {
  return router.get("/stats", async (request, response) => {
    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = payload.user.email;
    const user = await getUserByEmail(email);
    const numberOfCourses = await getNumberOfCourses(user.id);
    let userCollege: any = 0;

    if (user.institution) {
      userCollege = user.institution;
    }

    let totalXp = 0;

    await Promise.all(user.courseProgress.map((c) => (totalXp += c.totalXp)));

    response.status(200).send({
      id: user.id,
      createdAt: user.createdAt,
      username: user.username,
      offensive: user.offensive,
      numberOfCourses,
      college: userCollege,
      courses: user.courseProgress,
      totalXp,
    });
  });
};

export default getUserStats;
