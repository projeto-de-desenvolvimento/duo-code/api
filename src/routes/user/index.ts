const express = require("express");
const router = express.Router();

import getUserStats from "./getUserStats";
import insertCollege from "./insertCollege";

getUserStats(router);
insertCollege(router);

module.exports = router;
