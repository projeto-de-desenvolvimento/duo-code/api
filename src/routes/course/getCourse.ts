import getCourseById from "../../repository/course/getCourseById";

const getCourse = (router) => {
  return router.get("/get/:id", async (request, response) => {
    const courseId = request.params.id;
    const curso = await getCourseById(courseId);
    if (curso) {
      response.status(200).send(curso);
    } else {
      response.status(404);
    }
  });
};

export default getCourse;
