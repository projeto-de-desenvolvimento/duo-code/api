import getUserByEmail from "../../repository/user/getUserByEmail";
import getCourseProgress from "../../repository/courseProgress/getProgress";
import getModuleById from "../../repository/module/getModuleById";
import updateLevelAndXpCourseProgress from "../../repository/courseProgress/updateLevelAndXpCourseProgress";
import updateLastSessionTimeAndOffensive from "../../repository/user/updateLastSessionTimeAndOffensive";
import addInstitutionXp from "../../repository/institution/addXp";
import getCourseById from "../../repository/course/getCourseById";
import { getRepository } from "typeorm";
import { Certificate } from "../../entity/Certificate";
import updateFinishedCourseProgress from "../../repository/courseProgress/updateFinishedCourseProgress";

const jwt = require("jsonwebtoken");

const sessionEnd = (router) => {
  return router.post("/session/end", async (request, response) => {
    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = await payload.user.email;
    const user = await getUserByEmail(email);

    const totalErrors = request.body.totalErrors;
    const courseId = request.body.courseId;
    const moduleId = request.body.moduleId;
    const courseProgress = await getCourseProgress(user.id, courseId);
    const totalXpGained = getTotalXp(totalErrors);
    const module = await getModuleById(moduleId);
    const course = await getCourseById(courseId);

    updateLastSessionTimeAndOffensive(
      user.id,
      user.offensive,
      user.lastSession
    );

    let level = courseProgress.level;
    let newLevel = false;
    const totalXp = courseProgress.totalXp + totalXpGained;

    if (!courseProgress.finished) {
      const thisIsTheLastLevel = await thisIsTheLastModule(
        courseProgress,
        module
      );

      if (thisIsTheLastLevel) {
        await getRepository(Certificate)
          .createQueryBuilder()
          .insert()
          .into(Certificate)
          .values({ hash: makehash(60), user, course })
          .execute();

        await updateFinishedCourseProgress(user.id, course.id, true);
      }
    }

    if (module.level == courseProgress.level) {
      level = level + 1;
      newLevel = true;
    }

    await updateLevelAndXpCourseProgress(user.id, courseId, level, totalXp)
      .then(() => {
        if (user.institutionId) {
          addInstitutionXp(user.institution.id, totalXpGained)
            .then(() => {
              response.status(200).send({ totalXpGained, newLevel });
            })
            .catch((error) => {
              response.status(500).send();
              throw error;
            });
        } else {
          response.status(200).send({ totalXpGained, newLevel });
        }
      })
      .catch((error) => {
        response.status(500).send();
        throw error;
      });
  });
};

const getTotalXp = (totalErrors: number): number => {
  if (totalErrors >= 5) {
    return 10;
  } else {
    return 15 - totalErrors;
  }
};

const thisIsTheLastModule = async (courseProgress, module) => {
  const course: any = await getCourseById(courseProgress.course.id);
  let lastLevel = 1;

  await Promise.all(
    course.module.map((module) => {
      if (module.level > lastLevel) lastLevel = module.level;
    })
  );

  if (module.level == lastLevel) {
    return true;
  } else {
    return false;
  }
};

function makehash(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export default sessionEnd;
