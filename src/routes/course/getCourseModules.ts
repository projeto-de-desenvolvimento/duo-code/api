import getModules from "../../repository/course/getModules";

const getCourseModules = (router) => {
  return router.post("/modules", async (request, response) => {
    const courseId = await request.body.courseId;
    if (courseId != undefined) {
      const modules: any = await getModules(courseId);
      let level = 1;
      let temp = [];
      let formatedModules = [];
      temp.push(modules[0]);
      for (let x = 1; x < modules.length; x++) {
        if (modules[x].level > level) {
          formatedModules.push(temp);
          temp = [];
          temp.push(modules[x]);
          level++;
        } else {
          temp.push(modules[x]);
        }
        if (x + 1 == modules.length) {
          formatedModules.push(temp);
        }
      }
      response.status(200).send(formatedModules);
    } else {
      response.status(400);
    }
  });
};

export default getCourseModules;
