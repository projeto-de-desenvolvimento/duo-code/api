import getAllCourses from "../../repository/course/getAllCourses";

const getCourses = async (router) => {
  return router.get("/get", async (request, response) => {
    const cursos = await getAllCourses();
    response.send(cursos);
  });
};

export default getCourses;
