import { getRepository, getConnection, Connection } from "typeorm";
import { Course } from "../entity/Course";
import javascriptCourse from "./Courses/Javascript/javascriptCourse";
import pythonCourse from "./Courses/Python/pythonCourse";
import CPlus from "./Courses/CPlus/CPlus";
import createInstitutions from "./createInstitutions";

const seed = async () => {
  console.log("Verificando Seed");
  const dbTest = await getRepository(Course).createQueryBuilder().getMany();
  if (dbTest.length == 0) {
    javascriptCourse();
    pythonCourse();
    CPlus();
    createInstitutions();
  }
  return;
};

export default seed;
