import { getRepository } from "typeorm";
import { Module } from "../../../../../entity/Module";
import { Question } from "../../../../../entity/Question";

const introductionVariables = async (module) => {
  await insertQuestion(
    module,
    "Introdução as Variáveis",
    "Variáveis em Javascript são uma forma para se salvar dados que possam ser acessados e/ou manipulados posteriormente, independente do tipo desses dados, strings, números, booleanos, arrays, objetos ou funções.",
    true,
    "...",
    "...",
    0
  );
  await insertQuestion(
    module,
    "-",
    "JavaScript é uma linguagem dinâmica orientada a objetos.",
    true,
    "JavaScript é uma linguagem orientada a objetos!",
    "true",
    2
  );
  await insertQuestion(
    module,
    "-",
    " A JavaScript foi criada em...",
    true,
    "...",
    "1995*1917*2005*2018",
    1
  );
  await insertQuestion(
    module,
    "-",
    "JavaScript é uma das três principais tecnologias da World Wide Web.",
    true,
    "Juntamente com HTML e CSS, o JavaScript é uma das três principais tecnologias da World Wide Web. JavaScript permite páginas da Web interativas e, portanto, é uma parte essencial dos aplicativos da web. A grande maioria dos sites usa",
    "true",
    2
  );
  await insertQuestion(
    module,
    "-",
    "JavaScript não é utilizada do lado do servidor.",
    true,
    "Javascript é também bastante utilizada do lado do servidor através de ambientes como o node.js.",
    "false",
    2
  );
  await insertQuestion(
    module,
    "-",
    "JavaScript permite páginas web...",
    true,
    "...",
    "Interativas*Inclusivas*Coloridas*Mais leves",
    1
  );
};

const insertQuestion = async (
  module: Module,
  title: string,
  questionText: string,
  primary: boolean,
  tip: string,
  answer: string,
  format: number
) => {
  try {
    await getRepository(Question)
      .createQueryBuilder()
      .insert()
      .into(Question)
      .values({
        title,
        questionText,
        primary,
        tip,
        module,
        answer,
        format,
      })
      .execute();
  } catch (err) {
    console.log(err);
  }
};

export default introductionVariables;
