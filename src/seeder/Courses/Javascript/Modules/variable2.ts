import { getRepository, getConnection, Connection } from "typeorm";
import { Course } from '../../../../entity/Course'
import { Module } from '../../../../entity/Module'
import introductionQuestions from './Questions/introductionQuestions'

const introduction = async (course) => {
    await getRepository(Module).createQueryBuilder()
        .insert()
        .values({
            name: "Variáveis II",
            moduleImage: "https://i.ibb.co/nLYbkCV/javascript-module-variaveis2.png",
            level: 3,
            course,
        })
        .execute()
    let module = await getRepository(Module).createQueryBuilder().where("name = 'Variáveis II'").getOne()
    introductionQuestions(module)
}

export default introduction