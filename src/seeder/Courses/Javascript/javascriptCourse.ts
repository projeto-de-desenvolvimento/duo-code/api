import { getRepository } from "typeorm";
import { Course } from "../../../entity/Course";
import introduction from "./Modules/introduction";
import variable1 from "./Modules/variable1";
import functions from "./Modules/functions";
import variable2 from "./Modules/variable2";

const javascriptCourse = async () => {
  await getRepository(Course)
    .createQueryBuilder()
    .insert()
    .into(Course)
    .values({
      name: "Javascript",
      version: "0.1",
      logoImage: "https://i.ibb.co/RcK1dTf/javascript-logo.png",
    })
    .execute();
  const course = await getRepository(Course)
    .createQueryBuilder()
    .where("name = 'Javascript'")
    .getOne();
  await introduction(course);
  await variable1(course);
  await functions(course);
  await variable2(course);
};

export default javascriptCourse;
