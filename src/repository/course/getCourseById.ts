import { getRepository } from "typeorm";
import { Course } from "../../entity/Course";

const getCourseById = async (courseId) => {
  const course = await getRepository(Course)
    .createQueryBuilder("course")
    .where("course.id = :id", { id: courseId })
    .leftJoinAndSelect("course.module", "modules")
    .getOne();

  return course;
};

export default getCourseById;
