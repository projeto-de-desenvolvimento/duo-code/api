import { getRepository } from "typeorm";
import { Course } from "../../entity/Course";

const getAllCourses = async () => {
  const courses = await getRepository(Course).createQueryBuilder().getMany();
  return courses;
};

export default getAllCourses;
