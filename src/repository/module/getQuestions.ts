import { getRepository } from "typeorm";
import { Question } from "../../entity/Question";

const getQuestions = async (moduleId) => {
  const questions = await getRepository(Question)
    .createQueryBuilder()
    .where("moduleId = :id", { id: moduleId })
    .getMany();

  return questions;
};

export default getQuestions;
