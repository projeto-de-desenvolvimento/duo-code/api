import { getConnection } from "typeorm";
import { CourseProgress } from "../../entity/CourseProgress";

const updateLevelAndXpCourseProgress = async (
  userId,
  courseId,
  level,
  totalXp
) => {
  await getConnection()
    .createQueryBuilder()
    .update(CourseProgress)
    .set({
      level,
      totalXp,
    })
    .where("courseId = :courseId AND userId = :userId", {
      userId,
      courseId,
    })
    .execute();
};

export default updateLevelAndXpCourseProgress;
