import { getRepository } from "typeorm";
import { CourseProgress } from "../../entity/CourseProgress";

const getCourseProgress = async (userId, courseId) => {
  const courseProgress = await getRepository(CourseProgress)
    .createQueryBuilder("courseProgress")
    .where("userId = :userId AND courseId = :courseId", {
      userId,
      courseId,
    })
    .leftJoinAndSelect("courseProgress.course", "course")
    .getOne();

  return courseProgress;
};

export default getCourseProgress;
