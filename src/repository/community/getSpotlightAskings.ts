import { getRepository } from "typeorm";
import { Asking } from "../../entity/Asking";
const numberPerPage = 4;

const getSpotlightAskings = async (page = 1) => {
  if (!page) page = 1;

  const min = (page - 1) * numberPerPage;
  const max = min + numberPerPage;

  let askings: any = await getRepository(Asking)
    .createQueryBuilder("asking")
    .select()
    .orderBy("asking.createdAt", "DESC")
    .leftJoinAndSelect("asking.comments", "comments")
    .skip(min)
    .take(max)
    .getMany();

  const length: any = await getRepository(Asking)
    .createQueryBuilder("asking")
    .select()
    .orderBy("asking.createdAt", "DESC")
    .getCount();

  await askings.map((ask) => {
    ask.number_comments = ask.comments.length;
    ask.comments = undefined;
  });

  return { askings, totalPages: Math.floor(length / numberPerPage) };
};

export default getSpotlightAskings;
