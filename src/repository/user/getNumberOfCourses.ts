import getUserByEmail from "../../repository/user/getUserByEmail";
import getUserCourses from "../../repository/courseProgress/getUserCourses";
const jwt = require("jsonwebtoken");

const getNumberOfCourses = async (userId) => {
  const courses = await getUserCourses(userId);
  if (courses) {
    return courses.length;
  } else {
    return 0;
  }
};

export default getNumberOfCourses;
