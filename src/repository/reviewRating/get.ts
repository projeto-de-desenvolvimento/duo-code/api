import { getRepository } from "typeorm";
import { ReviewRating } from "../../entity/ReviewRating";

const getReviewRating = async (id) => {
  return getRepository(ReviewRating)
    .createQueryBuilder("reviewRating")
    .select()
    .where("reviewRating.id = :id", { id })
    .leftJoinAndSelect("reviewRating.user", "user")
    .leftJoinAndSelect("reviewRating.comment", "comment")
    .getOne();
};

export default getReviewRating;
