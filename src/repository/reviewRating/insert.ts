import { getRepository } from "typeorm";
import { ReviewRating } from "../../entity/ReviewRating";
import { User } from "../../entity/User";
import deleteReviewRating from "./delete";
import updateReviewRating from "./update";

const insertReviewRating = async (like, user, comment) => {
  let repeatUserComment = await getRepository(ReviewRating)
    .createQueryBuilder("review")
    .leftJoinAndSelect("review.user", "user")
    .leftJoinAndSelect("review.comment", "comment")
    .where("user.id = :userId && comment.id = :commentId", {
      userId: user.id,
      commentId: comment.id,
    })
    .getOne()
    .catch((err) => {
      console.log(err);
    });

  if (repeatUserComment) {
    if (repeatUserComment.like == like) {
      await deleteReviewRating(repeatUserComment.id);
      return;
    }

    repeatUserComment = { ...repeatUserComment, ...{ like } };
    await updateReviewRating(repeatUserComment);
    return;
  }

  await getRepository(ReviewRating)
    .createQueryBuilder()
    .insert()
    .into(ReviewRating)
    .values({
      like,
      user,
      comment,
    })
    .execute();
};

export default insertReviewRating;
