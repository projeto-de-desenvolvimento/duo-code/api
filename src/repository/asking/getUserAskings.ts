import { getRepository } from "typeorm";
import { Asking } from "../../entity/Asking";

const deleteAsking = async (userId) => {
  await getRepository(Asking)
    .createQueryBuilder("asking")
    .select()
    .leftJoinAndSelect("asking.user", "user")
    .where("user.id = :id", { id: userId })
    .getMany();
};

export default deleteAsking;
