import { getRepository } from "typeorm";
import { Asking } from "../../entity/Asking";

const getAsking = async (id) => {
  const ask = await getRepository(Asking)
    .createQueryBuilder("asking")
    .select()
    .where("asking.id = :id", { id })
    .leftJoinAndSelect("asking.user", "user")
    .leftJoinAndSelect("asking.comments", "comments")
    .leftJoinAndSelect("comments.user", "commentUser")
    .leftJoinAndSelect("comments.reviewRatings", "reviewRatings")
    .orderBy({ "comments.created_at": "DESC" })
    .getOne();

  await Promise.all(ask.comments.filter((comment) => comment.active));

  await Promise.all(
    ask.comments.map((comment: any) => {
      const positive = comment.reviewRatings.filter((r) => r.like == true);
      const negative = comment.reviewRatings.filter((r) => r.like == false);

      comment.reviewRatings = {
        positive: positive.length,
        negative: negative.length,
      };
    })
  );

  return ask;
};

export default getAsking;
