import { getRepository } from "typeorm";
import { Asking } from "../../entity/Asking";
import { User } from "../../entity/User";

const deleteAsking = async (data) => {
  await getRepository(Asking)
    .createQueryBuilder()
    .update(Asking)
    .set({
      title: data.title,
      tags: data.tags,
      content: data.content,
      active: data.active,
    })
    .where("id = :id", { id: data.id })
    .execute();
};

export default deleteAsking;
