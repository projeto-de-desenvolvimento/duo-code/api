import { getConnection } from "typeorm";
import { Institution } from "../../entity/Institution";

const newInstitution = async (name) => {
  await getConnection()
    .createQueryBuilder()
    .insert()
    .into(Institution)
    .values({
      name,
    })
    .execute();
};

export default newInstitution;
