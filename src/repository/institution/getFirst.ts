import { getRepository } from "typeorm";
import { Institution } from "../../entity/Institution";

const getFirst = async () => {
  const institution = await getRepository(Institution)
    .createQueryBuilder()
    .take(10)
    .getMany();
  return institution;
};

export default getFirst;
