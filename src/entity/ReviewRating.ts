import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  PrimaryColumn,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./User";
import { Comment } from "./Comment";

@Entity()
export class ReviewRating {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("boolean")
  like: boolean;

  @ManyToOne((type) => User, (user) => user.reviewRatings)
  user: User;

  @ManyToOne((type) => Comment, (comment) => comment.reviewRatings)
  comment: Comment;

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
