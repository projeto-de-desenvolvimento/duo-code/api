import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { Comment } from "./Comment";
import { Course } from "./Course";
import { User } from "./User";

@Entity()
export class Certificate {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", { length: 60 })
  hash: string;

  @ManyToOne((type) => User, (user) => user.certificates, { nullable: false })
  user: User;

  @ManyToOne((type) => Course, (course) => course.certificates, {
    nullable: false,
  })
  course: Course;

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
