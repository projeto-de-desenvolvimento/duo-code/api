import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { Course } from "./Course";
import { Question } from "./Question";
import { ModuleProgress } from "./ModuleProgress";

@Entity()
export class Module {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", { length: 40 })
  name: string;

  @Column("varchar", { length: 60, nullable: true })
  moduleImage: string;

  @Column("tinyint")
  level: number;

  @ManyToOne((type) => Course, (course) => course.module)
  course: Course;

  @OneToMany((type) => Question, (question) => question.module)
  questions: Question;

  @OneToMany(
    (type) => ModuleProgress,
    (moduleProgress) => moduleProgress.module
  )
  moduleProgress: ModuleProgress;

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
