import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { Certificate } from "./Certificate";
import { CourseProgress } from "./CourseProgress";
import { Module } from "./Module";

@Entity()
export class Course {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", { length: 40 })
  name: string;

  @Column("varchar", { length: 5000, nullable: true })
  description: string;

  @Column("varchar", { length: 16 })
  version: string;

  @Column("varchar", { length: 255 })
  logoImage: string;

  @OneToMany(
    (type) => CourseProgress,
    (courseProgress) => courseProgress.course
  )
  courseProgress: CourseProgress[];

  @OneToMany((type) => Module, (module) => module.course)
  module: Module[];

  @OneToMany((type) => Certificate, (certificate) => certificate.course)
  certificates: Certificate[];

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
