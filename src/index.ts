import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import seed from "./seeder/seed";
import middleware from "./routes/authentication/middleware";
import ormConfig from "./config/ormConfig";

const routeAuth = require("./routes/authentication/auth");
const routeMain = require("./routes/main");
const routeCourse = require("./routes/course/index");
const routeUser = require("./routes/user/index");
const routeCollege = require("./routes/college/index");
const routeRanking = require("./routes/ranking/index");
const routeAsking = require("./routes/asking/index");
const routeComment = require("./routes/comment/index");
const routeReviewRating = require("./routes/reviewRating/index");
const routeCommunity = require("./routes/community/index");
const routeCertificate = require("./routes/certificate/index");
require("dotenv/config");

const options: cors.CorsOptions = {
  allowedHeaders: [
    "Origin",
    "X-Requested-With",
    "Content-Type",
    "Accept",
    "X-Access-Token",
    "Authorization",
  ],
  credentials: true,
  methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  origin: process.env.ALLOW_CORS_WEB_URL,
  preflightContinue: false,
};

createConnection(ormConfig())
  .then(async (connection) => {
    const app = express();
    app.use(cors(options));
    app.options("*", cors(options));

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.use("/auth", routeAuth);
    app.use("/main", middleware, routeMain);
    app.use("/course", middleware, routeCourse);
    app.use("/user", middleware, routeUser);
    app.use("/college", middleware, routeCollege);
    app.use("/ranking", middleware, routeRanking);
    app.use("/asking", middleware, routeAsking);
    app.use("/comment", middleware, routeComment);
    app.use("/reviewRating", middleware, routeReviewRating);
    app.use("/community", middleware, routeCommunity);
    app.use("/certificate", middleware, routeCertificate);

    const port = process.env.PORT || 3200;
    app.listen(port);

    seed();
    console.log("Rodando na porta " + port);
  })
  .catch((error) => console.log(error));
