import getUserByEmail from "../src/repository/user/getUserByEmail";
const jwt = require("jsonwebtoken");

export default async function getUser(request) {
  const [, token] = request.headers.authorization.split(" ");
  const payload = jwt.verify(token, process.env.SECRET_KEY);
  const email = payload.user.email;
  const user = await getUserByEmail(email);

  return user;
}
